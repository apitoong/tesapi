//
//  tesApp.swift
//  tes
//
//  Created by afitra mamor on 11/11/20.
//

import SwiftUI

@main
struct tesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
