//
//  ContentView.swift
//  tes
//
//  Created by afitra mamor on 11/11/20.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var fetch = ApiServices()
 
    var body : some View {
        VStack{
     
            List(fetch.post){ post in
                    Text(post.title)
                        .font(.system(size:24, weight: .bold, design: .rounded))
                    Text(post.body)
                        .font(.system(size:16, weight: .bold, design: .rounded))
                    Button(action:  {print("ini id ke \(post.id)")}, label: {
                        Text("KLik saysa")
                    })
                
            }
        }
      
        
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
